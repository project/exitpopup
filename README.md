CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Exit Pop Up Module Help You To Convert The Leaving Visitor For Your Traffic
And Help Yo To Grow Your Business And Engaging Visitors. Whenever User Tries To
Leave Your Site A Pop Up Will Appear Displaying Offer, Or Anything That You
Would Like To Display.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/exitpopup

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/exitpopup


REQUIREMENTS
------------

This module doesn't require any module outside of Drupal core.


INSTALLATION
------------

 * Install this module as you would normally install a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * The Module Comes With Full Customisation Feature All You Need To Write Your Own
   Custom Html Template And Css Code.

   1. Copy the exitpopup folder to your modules directory.
   2. Download exitpopup library from
      https://github.com/beeker1121/exit-intent-popup.git.
   3. Copy the downloaded content that is exit-intent-popup-master folder in the
      root libraries folder (/libraries).
   4. Go to admin/modules and enable the module.
   5. Go To The Admin/config/exitpopup
   6. In Html Template Just Write Your Very Own Custom Html Template
   7. In Css Section You Need To Write The Css For The Code That You Have Written
      In Html Template
   8. You  Can Also Customize The Width And Height Of The Banner Eg Default
      Is 600 X 300 In Px
   9. You Can Also Delay The Popup Dispay In Second Default Is  0
   10. You Can Also Setup  The Cookie So That User Do Not Get Annoyed And Set
       After How Many Day  He Will Be Seeing The Banner
   11. You Can Also  Control The Display Of Use On The Basis Of There Role.
   12. Save The Configuration And Bingo You Are Done Now Popup Will Appears
       Whenever User Want To Leave The Site.



MAINTAINERS
-----------

 * Gaurav Kapoor - https://www.drupal.org/u/gauravkapoor
 * Tanuj Sharma - Https://www.drupal.org/u/badmetevils

Supporting organizations:

 * OpenSense Labs - https://www.drupal.org/opensense-labs
